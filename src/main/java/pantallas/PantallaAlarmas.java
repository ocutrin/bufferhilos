package pantallas;

import lombok.Getter;
import lombok.Setter;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import org.apache.log4j.Logger;

public class PantallaAlarmas {
    private static final Logger log = Logger.getLogger(PantallaAlarmas.class);

    @Getter
    @Setter
    private boolean isActiva = false;

    private final Stage stage = new Stage();

    @Getter
    private final ListView<String> listView = new ListView<>();

    public PantallaAlarmas() {
        iniciarScena();
    }

    void iniciarScena() {
        Button button1 = new Button("Eliminar seleccionada");
        button1.setOnMouseClicked(e -> {
            listView.getItems().remove(listView.getSelectionModel().getSelectedItem());
            // listView.getItems().stream().forEach(log::debug);
        });
        Button button2 = new Button("Cerrar");
        button2.setOnMouseClicked(e -> close());
        BorderPane borderPane = new BorderPane();
        HBox hBox = new HBox();
        hBox.getChildren().add(button1);
        hBox.getChildren().add(button2);
        borderPane.setCenter(listView);
        borderPane.setBottom(hBox);
        stage.setScene(new Scene(borderPane));
        stage.setOnCloseRequest(e -> isActiva = false);
    }

    public final void show() {
        setActiva(true);
        stage.show();

    }

    public final void close() {
        stage.close();
        setActiva(false);
    }

}
