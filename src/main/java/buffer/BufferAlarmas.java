package buffer;

import lombok.Getter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

public class BufferAlarmas {
    private static final Logger log = Logger.getLogger(BufferAlarmas.class);

    // Alarmas maximas que caben en el buffer.
    private static final int MAX_ALARMAS_BUFFER = 30;

    // Alarmas maximas que se puden extraer del buffer de una vez.
    private static final int MAX_ALARMAS_SACAR = 5;

    // Lista con las alarmas del buffer.
    @Getter
    private final Set<String> alarmas = new HashSet<>();

    // Metodo para meter alarmas en el buffer, este metodo sera utilizado por el hilo gestion
    // de maquina, que sera quien llene el buffer. Por seguridad se establece un maximo tamanio
    // de buffer MAX_ALARMAS_BUFFER.
    public synchronized void meter(List<String> alarmas) throws InterruptedException {

        // Comprovamos que el buffer no este lleno, de lo contrario ponemos en espera al hilo maquina ya que no puede meter mas alarmas.
        while (getAlarmas().size() >= MAX_ALARMAS_BUFFER) {
            log.debug("El buffer esta lleno. Hilo gestor de maquina pasa a la espera de que haya sitio para mas alarmas. buffer=" + getAlarmas().size());
            wait();
        }

        // Comprobamos que las nuevas alarmas que llegan de la maquina no estan ya en el buffer.
        AtomicBoolean hayNuevas = new AtomicBoolean(false);
        alarmas.forEach(an -> {
            if (getAlarmas().stream().filter(a -> a.equals(an)).count() == 0) {
                boolean addCorrecto = getAlarmas().add(an);
                if (!hayNuevas.get()) {
                    hayNuevas.set(addCorrecto);
                }
            }
        });

        // Si hay nuevas alarmas notificamos al hilo gestor de alarmas que han lleagado nuevas alarmas de la maquina.
        if (hayNuevas.get()) {
            // log.debug("Recibiendo nuevas alarmas... buffer=" + getAlarmas().size() + ": " + String.join(",", getAlarmas()));.
            notifyAll();
        }

    }

    // Metodo para sacar alarmas del buffer, este metodo sera utilizado por el hilo gestion
    // de alarmas que sera quien se nutra de los datos que vaya introduciendo el hilo maquina.
    // Devuelve una lista de alarmas con un tamanio maximo extablecido MAX_ALARMAS_SACAR.
    public synchronized List<String> sacar() throws InterruptedException {

        // Mientras no hayan alarmas en el buffer ponemos en espera al hilo gestor de alarmas.
        while (alarmas.isEmpty()) {
            log.debug("El buffer esta vacio. Hilo gestor de alarmas pasa a la espera de que hayan alarmas. buffer=" + getAlarmas().size());
            wait();
        }

        // Devolvemos alarmas del buffer..
        List<String> listAux = new ArrayList<>(getAlarmas().size() <= MAX_ALARMAS_SACAR ? getAlarmas() : new ArrayList<>(getAlarmas()).subList(0, MAX_ALARMAS_SACAR));
        getAlarmas().removeAll(listAux);

        // Notificamos que hemos sacado alarmas del buffer, por si el hilo maquina esta a la espera de que se vacie el buffer.
        notifyAll();

        // log.debug("Sacando alarmas... buffer=" + getAlarmas().size() + ": " + String.join(",", getAlarmas()));.

        // Retornamos las alarmas extraidas del buffer.
        return listAux;
    }

}
