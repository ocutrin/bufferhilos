package bufferhilos;

import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import buffer.BufferAlarmas;
import hilos.TareaGestionAlarmas;
import hilos.TareaGestionMaquina;
import pantallas.PantallaAlarmas;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Getter
    @Setter
    private static boolean esAppActiva = false;

    @Getter
    public static final ExecutorService executorServices = Executors.newFixedThreadPool(2);

    private static final BufferAlarmas bufferAlarmas = new BufferAlarmas();

    private static final PantallaAlarmas pantallaAlarmas = new PantallaAlarmas();

    private static final TareaGestionMaquina tareaGestionMaquina = new TareaGestionMaquina(bufferAlarmas, pantallaAlarmas);

    private static final TareaGestionAlarmas tareaGestionAlarmas = new TareaGestionAlarmas(bufferAlarmas, pantallaAlarmas);

    @Override
    public void start(Stage primaryStage) {

        // Iniciamos scena principal
        iniciarScena(primaryStage);

        primaryStage.setOnCloseRequest(e -> setEsAppActiva(false));

        setEsAppActiva(true);

        // Iniciamos hilo encargado de capturar alarmas de la maquina.
        executorServices.execute(tareaGestionMaquina);

        // Iniciamos hilo encargado de gestionar las alarmas en la aplicacion.
        executorServices.execute(tareaGestionAlarmas);

    }

    // Metodo crear pantalla principal.
    private static final void iniciarScena(Stage stage) {
        Button button = new Button("Alarmas");
        button.setOnMouseClicked(e -> pantallaAlarmas.show());
        BorderPane borderPane = new BorderPane();
        borderPane.setBottom(button);
        stage.setScene(new Scene(borderPane, 600, 400));
        stage.setTitle("Pantalla principal");
        stage.show();
    }

}
