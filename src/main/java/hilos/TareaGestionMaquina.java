package hilos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimerTask;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.log4j.Logger;

import buffer.BufferAlarmas;
import bufferhilos.Main;
import pantallas.PantallaAlarmas;

public class TareaGestionMaquina extends TimerTask {
    private static final Logger log = Logger.getLogger(TareaGestionMaquina.class);

    private static final int DELAY = 2000;

    private final BufferAlarmas bufferAlarmas;
    private final PantallaAlarmas pantallaAlarmas;

    public TareaGestionMaquina(BufferAlarmas bufferAlarmas, PantallaAlarmas pantallaAlarmas) {
        this.bufferAlarmas = bufferAlarmas;
        this.pantallaAlarmas = pantallaAlarmas;
    }

    @Override
    public void run() {
        log.info("Ejecutando hilo TareaGestionMaquina...");

        try {

            while (!Main.executorServices.isTerminated() && Main.isEsAppActiva()) {

                // Comprobamos si hemos recivido nuevas alarmas de la maquina.
                if (true && !new Random().nextBoolean()) {

                    Set<String> nuevasAlarmas = new HashSet<>();

                    // Generamos nuevas alarmas
                    IntStream.range(0, new Random().nextInt(1) + 1).forEach(i -> nuevasAlarmas.add(generarAlarma()));

                    // Si hay alarmas nuevas las metemos en el buffer.
                    List<String> listAux = filtramosAlarmaYaEnPantalla(nuevasAlarmas);
                    if (!listAux.isEmpty()) {

                        bufferAlarmas.meter(listAux);

                    }

                } else {
                    log.debug("Sin alarmas.");
                }

                Thread.sleep(DELAY);

            }

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        }

    }

    private final String generarAlarma() {
        String alarma = "A" + new Random().nextInt(50);
        log.debug("alarma generada: " + alarma);
        return alarma;
    }

    private List<String> filtramosAlarmaYaEnPantalla(Set<String> alarmas) {
        Map<String, String> map = pantallaAlarmas.getListView().getItems().stream().collect(Collectors.toMap(s -> s, s -> s));
        List<String> listAux = new ArrayList<>();
        alarmas.forEach((a) -> {
            if (!map.containsKey(a)) {
                listAux.add(a);
            }
        });
        return listAux;
    }

}
