package hilos;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;
import java.util.stream.Collectors;

import javafx.application.Platform;

import org.apache.log4j.Logger;

import buffer.BufferAlarmas;
import bufferhilos.Main;
import pantallas.PantallaAlarmas;

public class TareaGestionAlarmas extends TimerTask {
    private static final Logger log = Logger.getLogger(TareaGestionAlarmas.class);

    private final BufferAlarmas bufferAlarmas;
    private final PantallaAlarmas pantallaAlarmas;

    public TareaGestionAlarmas(BufferAlarmas bufferAlarmas, PantallaAlarmas pantallaAlarmas) {
        this.bufferAlarmas = bufferAlarmas;
        this.pantallaAlarmas = pantallaAlarmas;
    }

    @Override
    public void run() {
        log.info("Ejecutando hilo TareaGestionAlarmas...");

        try {

            while (!Main.executorServices.isTerminated() && Main.isEsAppActiva()) {

                // Alarmas que cojemos del buffer.
                Set<String> setAux1 = new HashSet<>(bufferAlarmas.sacar());

                // Insertamos nuevas alarmas a la lista de la pantalla.
                Platform.runLater(() -> insertarSiNoExiste(setAux1));

                // pantallaAlarmas.getListView().getItems().forEach(log::debug);.

                // Comprobamos si la pantalla alerta esta activa sino la lanzamos.
                if (!pantallaAlarmas.isActiva()) {
                    pantallaAlarmas.setActiva(true);
                    Platform.runLater(() -> pantallaAlarmas.show());
                }

            }

        } catch (Exception e) {
            Thread.currentThread().interrupt();
            log.error(e.getMessage(), e);
        }
    }

    private void insertarSiNoExiste(Set<String> alarmas) {
        Map<String, String> map = pantallaAlarmas.getListView().getItems().stream().collect(Collectors.toMap(s -> s, s -> s));
        alarmas.forEach((a) -> {
            if (!map.containsKey(a)) {
                pantallaAlarmas.getListView().getItems().add(a);
            }
        });
    }

}
